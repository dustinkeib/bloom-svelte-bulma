# Basic Svelte + Bulma Example



Everything you need to build a Svelte project, powered by [`Bulma`](https://bulma.io/documentation/overview/start/) and [`SvelteKit`](https://github.com/sveltejs/kit/tree/master/packages/create-svelte);

## Getting Started

Make sure you have [`NodeJS`](https://nodejs.org/) installed, then clone this repo.

```bash
git clone git@bitbucket.org:dustinkeib/svelte-bulma.git
```

Now, change into the directory and install dependancies:

```bash
npm install
```

## Developing

Once you've created a project and installed dependencies with `npm install` (or `pnpm install` or `yarn`), start a development server:

```bash
npm run dev

# or start the server and open the app in a new browser tab
npm run dev -- --open
```

## Building

Before creating a production version of your app, install an [adapter](https://kit.svelte.dev/docs#adapters) for your target environment. Then:

```bash
npm run build
```

> You can preview the built app with `npm run preview`, regardless of whether you installed an adapter. This should _not_ be used to serve your app in production.
